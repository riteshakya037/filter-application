package ui_elements;

/**
 * Accessory to {@link javax.swing.JFileChooser} which adds a side panel in which selected files are displayed.
 * @author Ritesh Shakya
 */

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;

class FileListAccessory extends JComponent implements PropertyChangeListener {

    private File file = null;
    private DefaultListModel<String> model;
    private JList<String> list;
    private java.util.List<File> fileList = new ArrayList<>();

    /**
     * @param chooser Parent {@link JFileChooser} into which accessory is to be added.
     */
    FileListAccessory(JFileChooser chooser) {
        chooser.addPropertyChangeListener(this);

        model = new DefaultListModel<>();
        list = new JList<>(model);
        JScrollPane pane = new JScrollPane(list);
        pane.setPreferredSize(new Dimension(200, 250));

        JButton removeItem = createRemoveItemButton();
        setLayout(new BorderLayout());
        add(pane);
        add(removeItem, BorderLayout.SOUTH);

    }

    /**
     * Add File to both List and Model.
     */
    private void addFileToList() {
        model.addElement(file.getName());
        fileList.add(file);
    }

    /**
     * Remove File to both List and Model.
     */
    private void removeFileFromList() {
        if (list.getSelectedIndex() != -1) {
            model.remove(list.getSelectedIndex());
            fileList.remove(list.getSelectedIndex());
        }
    }

    /**
     * @return Creates a single button to remove file.
     */
    private JButton createRemoveItemButton() {
        JButton button = new JButton("Remove");
        button.addActionListener(e -> removeFileFromList());
        return button;
    }

    /**
     * @param e Property change in {@link JFileChooser}. Only concerned with selection property change.
     */
    @Override
    public void propertyChange(PropertyChangeEvent e) {
        boolean update = false;
        String prop = e.getPropertyName();

        //If the directory changed, don't do anything
        if (JFileChooser.DIRECTORY_CHANGED_PROPERTY.equals(prop)) {
            file = null;
            update = true;
            //If a file became selected, find out which one.
        } else if (JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals(prop)) {
            file = (File) e.getNewValue();
            update = true;
        }

        if (update && file != null) {
            addFileToList();
        }
    }

    /**
     * @return List of files selected
     */
    public File[] getFiles() {
        File[] stockArr = new File[fileList.size()];
        return fileList.toArray(stockArr);
    }
}
