package ui_elements;

import constant_elements.AppProperties;
import utility_elements.PropertyUtilities;
import utility_elements.RunOption;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;

/**
 * Helper Class which helps the main UI
 *
 * @author Ritesh Shakya
 */
class UIHelper {

    /**
     * Method for creating a {@link JFileChooser} with varying conditions
     *
     * @param parent    The parent component
     * @param runOption Various options packed in a Interface
     */
    static void createFileChooser(Component parent, RunOption runOption) {
        JFileChooser chooser = new JFileChooser(new File(runOption.getLocation()));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt", "text", "csv");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Select Keyword Csv");
        chooser.setAcceptAllFileFilterUsed(false);
        FileListAccessory accessory = new FileListAccessory(chooser);
        chooser.setAccessory(accessory);

        if (chooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
            selectOption(runOption, accessory.getFiles());
        }
    }

    /**
     * Executes the command based on {@code runOption} provided
     *
     * @param runOption Various options packed in a Interface
     * @param chooser   Chooser Object used to select files
     */
    private static void selectOption(RunOption runOption, File[] chooser) {
        runOption.setLocation(chooser[0].getParent());
        runOption.execute(chooser);
    }

    /**
     * @param panel     Parent Component
     * @param isEnabled Enable/Disable children.
     */
    static void setPanelEnabled(JPanel panel, Boolean isEnabled) {
        panel.setEnabled(isEnabled);
        Component[] components = panel.getComponents();
        for (Component component : components) {
            if (component.getClass().getName().equals("javax.swing.JPanel")) {
                setPanelEnabled((JPanel) component, isEnabled);
            }
            component.setEnabled(isEnabled);
        }
    }

    /**
     * Method for creating a {@link JFileChooser} for Folder Selection
     *
     * @param parent The parent component
     */
    static void createFileChooser(Component parent, FolderSelection folderSelection) {
        JFileChooser chooser = new JFileChooser(new File(PropertyUtilities.getProperty(AppProperties.OUTPUT_FOLDER)));
        chooser.setDialogTitle("Select Folder");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        // disable the "All files" option.
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
            folderSelection.execute(chooser);
        }
    }

    interface FolderSelection {
        void execute(JFileChooser chooser);
    }

    public static class FilterFolder implements FolderSelection {

        @Override
        public void execute(JFileChooser chooser) {
            FilterPanel.enableImport();
            FilterPanel.setOutputDirectory(chooser.getSelectedFile().getAbsolutePath());
            PropertyUtilities.saveProperty(AppProperties.FILTER_FOLDER, String.valueOf(chooser.getSelectedFile()));
        }
    }

    public static class OutputFolder implements FolderSelection {

        @Override
        public void execute(JFileChooser chooser) {
            App.enableStart();
            App.setOutputDirectory(chooser.getSelectedFile().getAbsolutePath());
            PropertyUtilities.saveProperty(AppProperties.OUTPUT_FOLDER, String.valueOf(chooser.getSelectedFile()));
        }
    }
}
