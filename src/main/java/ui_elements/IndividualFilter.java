package ui_elements;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;

/**
 * Group for Displaying Filter File with option to select for processing.
 * @author Ritesh Shakya
 */
public class IndividualFilter extends JPanel {
    private JCheckBox classCheck;
    private JLabel keywordLbl;
    private int preferredHeight = 32;
    private JCheckBox checkUse;


    /**
     * @param filter String to display
     * @param useFor If {@code true} file is used in processing; {@code else} otherwise.
     */
    IndividualFilter(String filter, Boolean useFor) {
        setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
        setName(filter);
        classCheck = makeCheckbox(false, 40);
        keywordLbl = makeLabel(filter, 200);
        checkUse = makeCheckbox(useFor, 50);

        setSize(new Dimension(300, preferredHeight));
        setBorder(new EtchedBorder());
        setVisible(true);
    }

    /**
     * JCheckBox initializer.
     *
     * @param forUse If {@code true} file is used in processing; {@code else} otherwise.
     * @param width  Width of Component
     * @return Initialized JCheckBox
     */
    private JCheckBox makeCheckbox(boolean forUse, int width) {
        JCheckBox chk = new JCheckBox();
        chk.setSelected(forUse);
        chk.setPreferredSize(new Dimension(width, preferredHeight));
        add(chk);
        return chk;
    }

    /**
     * JLabel initializer.
     *
     * @param text  Text to Display.
     * @param width Width of Component
     * @return Initialized JLabel.
     */
    private JLabel makeLabel(String text, int width) {
        JLabel t = new JLabel(text);
        t.setPreferredSize(new Dimension(width, preferredHeight));
        add(t);
        t.setHorizontalAlignment(SwingConstants.LEFT);
        return t;
    }

    /**
     * @return {@code true} if File is selected for deletion; {@code false} otherwise.
     */
    boolean getSelected() {
        return classCheck.isSelected();
    }

    /**
     * @param select if {@code true} set file for deletion; {@code false} otherwise.
     */
    void setSelected(boolean select) {
        classCheck.setSelected(select);
    }

    /**
     * @return Get File name.
     */
    public String getKeyword() {
        return keywordLbl.getText();
    }

    /**
     * @return {@code true} if File is selected for Processing; {@code false} otherwise.
     */
    public Boolean getUseFor() {
        return checkUse.isSelected();
    }
}
