package ui_elements;

import constant_elements.AppProperties;
import process_elements.ImportFilter;
import utility_elements.FileUtilities;
import utility_elements.ListenerMap;
import utility_elements.PropertyUtilities;
import utility_elements.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

/**
 * Panel for Filter Management.
 *
 * @author Ritesh Shakya
 */
public class FilterPanel extends JDialog {
    private static FilterPanel instance = null;
    /**
     * Main Content of JDialog.
     */
    private JPanel contentPane;
    /**
     * Button that selects all Filter Files.
     */
    private JButton buttonSelectAll;
    /**
     * Button that deselects all Filter Files.
     */
    private JButton buttonDeselect;
    /**
     * Button that removes all Filter Files.
     */
    private JButton buttonRemove;
    /**
     * Main Panel in which Filter Files are shown.
     */
    private JPanel insideScrollPanel;
    /**
     * Import new Filter Files.
     */
    private JButton importFilter;
    /**
     * Shows the current Filter Files count.
     */
    private JLabel countLabel;
    /**
     * Shows status while saving Filter Files.
     */
    private JLabel statusLabel;
    /**
     * Button to save changes.
     */
    private JButton buttonSave;
    /**
     * Button to select Filter Folder
     */
    private JButton selectFilterFolder;
    /**
     * Shows the current filter Folder.
     */
    private JTextField filterFolderTxt;
    /**
     * Map of Filter Files with bool for use in processing. Also has action listener that adds or deletes UI on element change.
     */
    public static ListenerMap<String, Boolean> filterList = new ListenerMap<>();
    /**
     * Contains the list of files that are removed when changes are seaved.
     */
    private java.util.List<String> removeList = new ArrayList<>();

    FilterPanel() {
        if (instance == null)
            instance = this;
        setContentPane(contentPane);
        setModal(true);
        setTitle("Filter Manager");
        getRootPane().setDefaultButton(importFilter);
        filterFolderTxt.setText(PropertyUtilities.getProperty(AppProperties.FILTER_FOLDER));
        filterList = FileUtilities.readFilterFiles();
        filterList.addPropertyChangeListener(evt -> changeFilter());
        filterList.removePropertyChangeListener(evt -> changeFilter());
        createFilters();
        buttonSelectAll.addActionListener(e -> onSelectAll());
        buttonDeselect.addActionListener(e -> onDeselectAll());
        buttonRemove.addActionListener(e -> onDelete());
        importFilter.addActionListener(e -> importFilter());
        if (StringUtils.isNull(filterFolderTxt.getText())) {
            importFilter.setEnabled(false);
        }
        buttonSave.addActionListener(e -> saveChanges());
        selectFilterFolder.addActionListener(e -> selectFilterFolder());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onClose();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onClose(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        setSize(new Dimension(340, 400));
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    private void selectFilterFolder() {
        UIHelper.createFileChooser(this, new UIHelper.FilterFolder());
    }

    /**
     * Opens a {@link JOptionPane} and saves all the changes on action.
     */
    private void saveChanges() {
        int option = JOptionPane.showConfirmDialog(null, "Save Changes", "Close", JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.YES_OPTION) {
            UIHelper.setPanelEnabled(contentPane, false);
            statusLabel.setText("Saving Changes");
            FileUtilities.writeFilterFiles(insideScrollPanel.getComponents());
            removeList.forEach(FileUtilities::deleteFilter);
            statusLabel.setText("");
            UIHelper.setPanelEnabled(contentPane, true);
        }
        dispose();
    }

    /**
     * Reloads UI for change in Filter File list.
     */
    private void changeFilter() {
        deleteAll();
        createFilters();
    }

    /**
     * Removes all Filter Files from UI.
     */
    private void deleteAll() {
        for (Component component : insideScrollPanel.getComponents()) {
            insideScrollPanel.remove(component);
            insideScrollPanel.revalidate();
            insideScrollPanel.repaint();
        }
    }

    /**
     * Provides a {@link JFileChooser} for importing Filters.
     */
    private void importFilter() {
        UIHelper.createFileChooser(this, new ImportFilter());
    }


    /**
     * Sets the total no. of Filter Files.
     */
    private void updateCount() {
        countLabel.setText("Total Filters Files : " + insideScrollPanel.getComponents().length);
    }

    /**
     * Adds all the Filter Files to UI.
     */
    private void createFilters() {
        insideScrollPanel.setLayout(new GridLayout(0, 1));
        filterList.forEach(this::addFilters);
    }

    /**
     * Adds A single Filter File with specific values.
     *
     * @param fileName Name of Filter File.
     * @param useFor   If {@code true} file is used in processing; {@code else} otherwise.
     * @see IndividualFilter
     */
    private void addFilters(String fileName, Boolean useFor) {
        insideScrollPanel.add(new IndividualFilter(fileName, useFor));
        insideScrollPanel.revalidate();
        insideScrollPanel.repaint();
        updateCount();
    }

    /**
     * Deselect all Filter Files.
     */
    private void onDeselectAll() {
        for (Component component : insideScrollPanel.getComponents()) {
            IndividualFilter individualFilter = (IndividualFilter) component;
            individualFilter.setSelected(false);
        }
    }

    /**
     * Select all Filter Files.
     */
    private void onSelectAll() {
        for (Component component : insideScrollPanel.getComponents()) {
            IndividualFilter individualFilter = (IndividualFilter) component;
            individualFilter.setSelected(true);
        }
    }

    /**
     * Delete selected Filter Files.
     */
    private void onDelete() {
        for (Component component : insideScrollPanel.getComponents()) {
            IndividualFilter individualFilter = (IndividualFilter) component;
            if (individualFilter.getSelected()) {
                filterList.remove(individualFilter.getKeyword());
                removeList.add(individualFilter.getKeyword());
                insideScrollPanel.remove(component);
                insideScrollPanel.revalidate();
                insideScrollPanel.repaint();
            }
        }
    }

    /**
     * Dispose Filter Manager.
     */
    private void onClose() {
        saveChanges();
        dispose();
    }


    /**
     * Used for testing.
     *
     * @param args {@code NULL}
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ignored) {
        }
        new FilterPanel();
    }


    public static void setOutputDirectory(String filterFolder) {
        instance.filterFolderTxt.setText(filterFolder);
    }

    public static void enableImport() {
        instance.importFilter.setEnabled(true);
    }
}
