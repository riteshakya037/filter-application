package ui_elements;


import constant_elements.AppProperties;
import process_elements.CreateFilterFile;
import process_elements.ImportFilter;
import process_elements.ImportInput;
import utility_elements.FileUtilities;
import utility_elements.PropertyUtilities;
import utility_elements.StringUtils;

import javax.swing.*;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

import static utility_elements.MavenProperties.getProjectProperties;


/**
 * Main UI for Application
 *
 * @author Ritesh Shakya
 */
public class App extends JFrame {
    /**
     * Main Content of JFrame.
     */
    private JPanel contentPane;
    /**
     * Button For bringing up Filter Management
     *
     * @see FilterPanel
     */
    private JButton buttonFilter;
    /**
     * Button for uploading input files.
     */
    private JButton buttonUpload;
    /**
     * Shows the output location.
     */
    private JTextArea outputLocationText;
    /**
     * Button for selecting output location.
     */
    private JButton buttonOutput;
    /**
     * Button for starting processing.
     */
    private JButton buttonStart;
    /**
     * Shows current status of program.
     */
    private JLabel programStatusLabel;
    /**
     * Shows current status of file under processing.
     */
    private JLabel statusLabel;
    /**
     * Current instance of {@link App}
     */
    private static App instance;
    /**
     * All files uploaded are stored in this variable.
     */
    public static Set<File> filesUploaded = new HashSet<>();

    /**
     * Initializes all variables and assigns action listeners
     */
    public App() {
        super(getProjectProperties("name") + " " + getProjectProperties("version"));
        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonUpload);

        buttonFilter.addActionListener(e -> manageFilters());
        buttonUpload.addActionListener(e -> uploadFiles());
        buttonOutput.addActionListener(e -> selectOutputFolder());
        buttonStart.addActionListener(e -> startProcess());
        programStatusLabel.setText("Initialization Complete");
        outputLocationText.setText(PropertyUtilities.getProperty(AppProperties.OUTPUT_FOLDER));
        buttonStart.setEnabled(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Starts Processing disabling all UI.
     */
    private void startProcess() {
        if (StringUtils.isNull(PropertyUtilities.getProperty(AppProperties.FILTER_FOLDER))) {
            statusText("Please initialize Filters", "");
        } else {
            UIHelper.setPanelEnabled(contentPane, false);
            statusText("Collecting Filter Files", "");
            FileUtilities.setLog();
            CreateFilterFile createFilterFile = new CreateFilterFile(filesUploaded);
            createFilterFile.execute();
        }
    }

    /**
     * Re-enables all UI and cleanups.
     */
    private void reEnableUI() {
        System.out.close();
        filesUploaded.clear();
        UIHelper.setPanelEnabled(contentPane, true);
        buttonStart.setEnabled(false);
    }

    /**
     * Sets status texts.
     *
     * @param programStatusText Text to set to Program Status.
     * @param fileStatusText    Text to set for File status.
     */
    private void statusText(String programStatusText, String fileStatusText) {
        if (programStatusText != null)
            programStatusLabel.setText(programStatusText);
        if (fileStatusText != null)
            statusLabel.setText(fileStatusText);
    }


    /**
     * Select Output Folder.
     */
    private void selectOutputFolder() {
        UIHelper.createFileChooser(this, new UIHelper.OutputFolder());
    }

    /**
     * File Chooser for processing
     *
     * @see JFileChooser
     * @see ImportInput
     */
    private void uploadFiles() {
        UIHelper.createFileChooser(this, new ImportInput());
        if (StringUtils.isNotNull(outputLocationText.getText())) {
            buttonStart.setEnabled(true);
        }
    }

    /**
     * Filter Panel which acts as filter for processing
     *
     * @see JFileChooser
     * @see ImportFilter
     */
    private void manageFilters() {
        new FilterPanel();
    }

    public static void main(String[] args) {
        //Setting UI as default
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        SwingUtilities.invokeLater(() -> instance = new App());
    }

    /**
     * @return Output Folder for processing.
     */
    public static String getOutputDirectory() {
        return instance.outputLocationText.getText();
    }

    /**
     * @param directory Sets Output Folder Processing.
     */
    static void setOutputDirectory(String directory) {
        instance.outputLocationText.setText(directory);
    }

    /**
     * Enables start button.
     */
    static void enableStart() {
        if (filesUploaded.size() != 0)
            instance.buttonStart.setEnabled(true);
    }

    /**
     * Static method that sets program status by calling its instance
     *
     * @param programStatusText Text to set to Program Status.
     * @param fileStatusText    Text to set for File status.
     * @see #statusText(String, String)
     */
    public static void setText(String programStatusText, String fileStatusText) {
        instance.statusText(programStatusText, fileStatusText);
    }

    /**
     * Static method that cleans up by calling its instance
     *
     * @see #reEnableUI()
     */
    public static void cleanUP() {
        instance.reEnableUI();
    }

}
