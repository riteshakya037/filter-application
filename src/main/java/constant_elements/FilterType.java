package constant_elements;

import utility_elements.FilterTypeMatcher;
import utility_elements.Patterns;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Possible type of Filter Files
 * @author Ritesh Shakya
 */
public enum FilterType {
    /**
     * Email line type.
     */
    EMAIL(Patterns.EMAIL_ADDRESS),
    /**
     * MD5 line type.
     */
    MD5(Patterns.MD5),
    /**
     * Domain line type.
     */
    DOMAIN(Patterns.DOMAIN_NAME),
    /**
     * Default line Type (To be ignored).
     */
    UNKNOWN(Patterns.UNKNOWN);

    private Pattern pattern;

    FilterType(Pattern pattern) {
        this.pattern = pattern;
    }

    public Pattern getPattern() {
        return pattern;
    }


    /**
     * Checks string with all {@link FilterType} and returns the one matching.
     * All the non matching goes to {@link FilterType#UNKNOWN}
     *
     * @param s Incoming string
     * @return FilterType which matches incoming String
     */
    public static FilterType match(String s) {
        for (FilterType filterType : Arrays.asList(FilterType.values())) {
            FilterType t = new FilterTypeMatcher(filterType).match(s);
            if (t != null) {
                return t;
            }
        }
        return FilterType.UNKNOWN;
    }
}
