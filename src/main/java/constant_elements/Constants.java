package constant_elements;

import java.io.File;

/**
 * Collection of various constants.
 * @author Ritesh Shakya
 */
public class Constants {
    /**
     * No of active threads in a pool
     */
    public static final int THREAD_POOL_SIZE = 50;
    /**
     * Folder Name for Log Files
     */
    public static final String LOG_DIRECTORY = File.separator + "Logs" + File.separator;
    /**
     * Folder Name for Failed Files
     */
    public static final String FAILED_DIRECTORY = File.separator + "Failed" + File.separator;
    public static final String TEMP_INPUT = File.separator + "Temp Input" + File.separator;
}
