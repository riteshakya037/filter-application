package constant_elements;

/**
 * Enum Key for Program Properties to store
 *
 * @author Ritesh Shakya
 */
public enum AppProperties {
    /**
     * Input Location of Extraction
     */
    INPUT_LOCATION("inputLocation"),
    /**
     * Filter Input Location of Extraction
     */
    FILTER_LOCATION("filterLocation"),
    /**
     * Output Location of Extraction
     */
    OUTPUT_FOLDER("outputFolder"),
    /**
     * Folder to keep the filter Files.
     */
    FILTER_FOLDER("filterFolder");

    private final String value;

    /**
     * @param value Property Name for key
     */
    AppProperties(String value) {
        this.value = value;
    }

    /**
     * @return key used to store property
     */
    public String getValue() {
        return value;
    }
}
