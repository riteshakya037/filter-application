package process_elements;

import constant_elements.Constants;
import ui_elements.App;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

/**
 * Processes file by creating a thread for each line.
 *
 * @author Ritesh Shakya
 */
class ProcessInputThread {
    /**
     * Asynchronous Thread pool to run each row for processing
     */
    private ExecutorService pool;
    /**
     * No of Lines in a files. Add to this as processing starts for each row.
     */
    private long lineCount = 0;
    /**
     * Time at which processing for this file started
     */
    private long start;
    /**
     * Input File for current processing.
     */
    private File filePath;

    private int processAtTime = 10000;

    /**
     * Initializes Thread pool with a size of {@link Constants#THREAD_POOL_SIZE}
     */
    ProcessInputThread() {
        pool = Executors.newFixedThreadPool(Constants.THREAD_POOL_SIZE);
    }

    /**
     * Checks the file for validity and starts processing based on it
     *
     * @param filePath Input File
     * @throws Exception All kind of exception for debugging
     */
    void buildSourceTapsAndPipes(File filePath) throws Exception {
        this.filePath = filePath;
        NumberFormat formatter = new DecimalFormat("#0.00000");
        start = System.currentTimeMillis();
        try (Stream<String> lines = Files.lines(Paths.get(filePath.getAbsolutePath()), StandardCharsets.UTF_8)) {
            lineCount = Files.lines(Paths.get(filePath.getAbsolutePath()), StandardCharsets.UTF_8).count();
            for (String line : (Iterable<String>) lines::iterator) {
                synchronized (this) {
                    pool.execute(new CheckForValidity(line.trim(), filePath));
                }
                if (processAtTime == 0) {
                    pool.shutdown();
                    while (!pool.isTerminated()) {
                        App.setText(null, formatter.format(CheckForValidity.getLinesDone() * 100.00 / lineCount) + "% Done");
                        Thread.sleep(100);
                    }
                    pool = Executors.newFixedThreadPool(Constants.THREAD_POOL_SIZE);
                    processAtTime = 10000;
                }
                processAtTime--;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        pool.shutdown();
        while (!pool.isTerminated()) {
            App.setText(null, formatter.format(CheckForValidity.getLinesDone() * 100.00 / lineCount) + "% Done");
            Thread.sleep(100);
        }
        cleanup();
    }

    /**
     * Cleans up after completion by resetting all variables and providing Updates.
     */
    private void cleanup() {
        CheckForValidity.cleanUP();
        NumberFormat formatter = new DecimalFormat("#0.00000");
        System.out.println("Finished \"" + filePath.getName() + "\" in " + formatter.format((System.currentTimeMillis() - start) / 1000d) + " seconds");
        lineCount = 0;
        CheckForValidity.setLinesDone(0);
    }
}