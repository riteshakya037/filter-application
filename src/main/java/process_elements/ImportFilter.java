package process_elements;

import constant_elements.AppProperties;
import ui_elements.FilterPanel;
import utility_elements.FileUtilities;
import utility_elements.PropertyUtilities;
import utility_elements.RunOption;

import javax.swing.*;
import java.io.File;

/**
 * Provides all needed information and execution for Input Filter
 * @author Ritesh Shakya
 */
public class ImportFilter extends SwingWorker<Void, Void> implements RunOption {

    /**
     * List of files Filter Files
     */
    private File[] data;

    /**
     * @param data Files array from the selected {@link JFileChooser#getSelectedFiles()}
     */
    public void execute(File[] data) {
        this.data = data;
        execute();
    }

    /**
     * @return Location of Filter Folder used previously
     */
    public String getLocation() {
        return PropertyUtilities.getProperty(AppProperties.FILTER_LOCATION);
    }

    /**
     * @param currentDirectory Location of current processing
     */
    public void setLocation(String currentDirectory) {
        PropertyUtilities.saveProperty(AppProperties.FILTER_LOCATION, currentDirectory);
    }

    /**
     * Imports the Files(by copying them to Filter Files Folder) and keeps file names in Filter data store.
     *
     * @return {@code NULL}
     * @throws Exception All exception to Print in log File
     */
    @Override
    protected Void doInBackground() throws Exception {
        for (File file : data) {
            FileUtilities.copyToFilter(file);
            FilterPanel.filterList.put(file.getName(), true);
        }

        return null;
    }

}
