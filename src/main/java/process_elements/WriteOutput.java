package process_elements;

import ui_elements.App;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

/**
 * Singleton class for  writing input rows.
 * Two instances are created, one for valid file types and other for invalid.
 * Each instance has its own {@link OutputStream} initiated which writes each row with synchronization.
 *
 * @author Ritesh Shakya
 */
public class WriteOutput {
    /**
     * Stores a separate instance for each condition along with respective {@link OutputStream}
     */
    private static HashMap<Boolean, WriteOutput> instanceMap = new HashMap<>();
    /**
     * {@link OutputStream} for each condition
     */
    private OutputStream out = null;
    /**
     * Condition of Validity
     */
    private boolean valid;
    /**
     * Total no of lines written.
     */
    static long countDone = 0;

    /**
     * @param filePath Input File
     * @param valid    Condition of validity.
     * @throws IOException When {@link OutputStream} could not be initiated.
     */
    private WriteOutput(File filePath, boolean valid) throws IOException {
        this.valid = valid;
        String outputDirectory = App.getOutputDirectory() + File.separator + (valid ? "Good_" : "Dirty_") + filePath.getName().replaceAll("\\s", "");
        out = Files.newOutputStream(Paths.get(outputDirectory), CREATE);
    }

    /**
     * @param filePath Input File to process
     * @param valid    Creates a Singleton for each valid and invalid cases and saves all instances to a map {@link #instanceMap}
     * @return A single instance for each condition of line.
     * @throws IOException If output file is not Found
     */
    public static WriteOutput getInstance(File filePath, boolean valid) throws IOException {
        synchronized (WriteOutput.class) {
            if (!instanceMap.containsKey(valid)) {
                instanceMap.put(valid, new WriteOutput(filePath, valid));
            }
        }
        return instanceMap.get(valid);
    }

    /**
     * @param joiner Output row received from {@link CheckForValidity} after Checking for Validity
     * @throws IOException If output file is not Found
     */
    public void write(String joiner) throws IOException {
        synchronized (instanceMap.get(valid)) {
            out.write((joiner + System.lineSeparator()).getBytes());
            countDone++;
        }
    }

    /**
     * Cleans up after completion by resetting all variables and closing all {@link OutputStream}
     */
    static void cleanUP() {
        for (WriteOutput writeFilter : instanceMap.values()) {
            try {
                writeFilter.out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        instanceMap.clear();
    }
}
