package process_elements;

import constant_elements.FilterType;
import ui_elements.App;
import utility_elements.FileUtilities;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Processes uploaded files one at a time.
 * Updates the UI as required to give status reports.
 *
 * @author Ritesh Shakya
 */
public class ProcessInput extends SwingWorker<Void, File> {
//    static HashMap<FilterType, List<String>> filterMap = new HashMap<>();
    private Set<File> filesUploaded;
    private File currentFile;
    /**
     * Time at which processing started
     */
    private final long start;
    /**
     * Flag to see if all files were extracted successfully
     * If {@code false} error in one or more file
     */
    private static boolean success = true;

    /**
     * Initializes Class as well as imports the Filter Lists.
     *
     * @param filesUploaded Files selected for Upload
     */
    public ProcessInput(Set<File> filesUploaded) {
        start = System.currentTimeMillis();
        this.filesUploaded = filesUploaded;
//        for (FilterType filterType : FilterType.values()) {
//            if (filterType != FilterType.UNKNOWN) {
//                App.setText("Creating Filter List", filterType.name());
//                filterMap.put(filterType, FileUtilities.readFilters(filterType));
//            }
//        }
    }

    /**
     * Iterates through each file for processing. Also updates UI accordingly.
     * Failed files are moved to Output Directory as Failed.
     * Note: One failure doesn't stop the process.
     *
     * @return {@code NULL}
     * @throws Exception Throws all Exception.
     * @see ProcessInputThread
     */
    @Override
    protected Void doInBackground() throws Exception {
        int fileNo = 1;
        for (File file : filesUploaded) {
            try {
                publish(file);
                App.setText("Working on " + fileNo++ + "/" + filesUploaded.size() + " - " + file.getName(), "Initializing...");
                (new ProcessInputThread()).buildSourceTapsAndPipes(file);
            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.out.println("Failed File: " + currentFile.getName());
                FileUtilities.moveFailed(currentFile.getAbsolutePath());
                success = false;
            }
        }
        return null;
    }


    /**
     * Cleans Up and re-enables UI
     */
    @Override
    protected void done() {
        NumberFormat formatter = new DecimalFormat("#0.00");
        App.setText("Extraction Completed in " + formatter.format((System.currentTimeMillis() - start) / 1000d) + " seconds" + (ProcessInput.success ? "" : " with failure."), "");
        App.cleanUP();
        try {
            Desktop.getDesktop().open(new File(App.getOutputDirectory()));
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    /**
     * Used to get Current File in case of exception.
     *
     * @param chunks List contains all the completed files as well as currently running file.
     */
    @Override
    protected void process(List<File> chunks) {
        currentFile = chunks.get(chunks.size() - 1);
    }
}
