package process_elements;

import constant_elements.AppProperties;
import constant_elements.Constants;
import constant_elements.FilterType;
import utility_elements.FileUtilities;
import utility_elements.ListenerMap;
import utility_elements.PropertyUtilities;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

/**
 * Creates Filter Lists for processing.
 * Each Type of {@link FilterType} creates a separate file.
 *
 * @author Ritesh Shakya
 */
public class CreateFilterFile extends SwingWorker<Void, Void> {
    /**
     * Location to store the Files uploaded as filter.
     */
    private static final String filterFolder = PropertyUtilities.getProperty(AppProperties.FILTER_FOLDER) + File.separator + "Filter Files" + File.separator;

    /**
     * Asynchronous Thread pool to run each row for processing
     */
    private final ExecutorService pool = Executors.newFixedThreadPool(Constants.THREAD_POOL_SIZE);
    private Set<File> filesUploaded;

    public CreateFilterFile(Set<File> filesUploaded) {
        this.filesUploaded = filesUploaded;
    }

    /**
     * Iterates through all selected Filter files to create Filter Lists
     *
     * @return {@code NULL}
     * @throws Exception All kinds on exception
     */
    @Override
    protected Void doInBackground() throws Exception {
        ListenerMap<String, Boolean> filterList = FileUtilities.readFilterFiles();
        FileUtilities.removeFilterList();
        filterList.entrySet().stream().filter(fileSet -> fileSet.getValue()).forEach(fileSet -> {
            try (Stream<String> lines = Files.lines(Paths.get(filterFolder + fileSet.getKey()), StandardCharsets.UTF_8)) {
                for (String line : (Iterable<String>) lines::iterator) {
                    pool.execute(new CheckFileType(line.trim()));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        pool.shutdown();
        while (!pool.isTerminated()) {
            Thread.sleep(100);
        }
        WriteFilter.instanceMap.values().forEach(WriteFilter::close);
        WriteFilter.instanceMap.clear();
        return null;
    }

    @Override
    protected void done() {
        new ProcessInput(filesUploaded).execute();
    }

    /**
     * Classifies the {@code line} into [MD5, Email, Domain]
     *
     * @see FilterType
     */
    private class CheckFileType implements Runnable {
        private String line;

        /**
         * @param line Single line from File read
         */
        CheckFileType(String line) {
            this.line = line;
        }

        @Override
        public void run() {
            try {
                WriteFilter.getInstance(FilterType.match(line)).write(line);
            } catch (IOException e) {
                e.printStackTrace(System.out);
            }
        }
    }
}
