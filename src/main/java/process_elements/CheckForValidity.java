package process_elements;

import constant_elements.FilterType;
import utility_elements.FileUtilities;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.stream.Stream;

/**
 * Checks Each row of input for validity
 *
 * @author Ritesh Shakya
 */
class CheckForValidity implements Runnable {
    private String line;
    private File filePath;

    /**
     * @param line     Input line
     * @param filePath Input File
     */
    CheckForValidity(String line, File filePath) {
        this.line = line;
        this.filePath = filePath;
    }

    /**
     * @param linesDone Set the no. of lines completed. Used for clearing by sending {@code 0}
     */
    static void setLinesDone(int linesDone) {
        WriteOutput.countDone = linesDone;
    }

    /**
     * Checks line for validity and calls respective instance of {@link WriteOutput}
     *
     * @see WriteOutput
     */
    @Override
    public void run() {
        try {
            if (checkMatchesFilter()) {
                WriteOutput.getInstance(filePath, false).write(line);
            } else {
                WriteOutput.getInstance(filePath, true).write(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * @return {@code true} if line is found in any of the file types; {@code false} otherwise.
     */
    private boolean checkMatchesFilter() {
        Stream<String> stringStream = FileUtilities.readFilters(FilterType.DOMAIN);
        for (String domain : (Iterable<String>) stringStream::iterator) {
            if (line.contains(domain)) {
                stringStream.close();
                return true;
            }
        }
        stringStream = FileUtilities.readFilters(FilterType.EMAIL);
        for (String email : (Iterable<String>) stringStream::iterator) {
            if (line.equalsIgnoreCase(email)) {
                stringStream.close();
                return true;
            }
        }
        stringStream = FileUtilities.readFilters(FilterType.MD5);
        for (String md5 : (Iterable<String>) stringStream::iterator) {
            if (line.equals(md5(md5))) {
                stringStream.close();
                return true;
            }
        }
        stringStream.close();
        return false;
    }

    /**
     * @param s String to be converted to MD5 hash.
     * @return MD5 of hashed string
     */
    private String md5(String s) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(s.getBytes(), 0, s.length());
            BigInteger i = new BigInteger(1, m.digest());
            return String.format("%1$032x", i);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return Total no of lines successfully written.
     */
    static long getLinesDone() {
        return WriteOutput.countDone;
    }

    /**
     * Cleanup After each file is done
     */
    static void cleanUP() {
        WriteOutput.cleanUP();
    }
}
