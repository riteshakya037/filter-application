package process_elements;

import constant_elements.AppProperties;
import constant_elements.FilterType;
import utility_elements.PropertyUtilities;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static java.nio.file.StandardOpenOption.CREATE;
import static utility_elements.MavenProperties.getProjectProperties;

/**
 * Singleton class for each filter line.
 * Each FilterType is a assigned a singleton with a {@link OutputStream} initiated.
 * On each new filter filter a new row is written to {@link OutputStream}.
 *
 * @author Ritesh Shakya
 */
class WriteFilter {
    /**
     * Default location to store program settings
     */
    private static final String programSettingFolder = PropertyUtilities.getProperty(AppProperties.FILTER_FOLDER) + File.separator;
    /**
     * Stores a separate instance for each {@link FilterType} along with respective {@link OutputStream}
     */
    static HashMap<FilterType, WriteFilter> instanceMap = new HashMap<>();
    /**
     * List of all Filters for current {@link FilterType}
     */
    private Set<String> existingList = new HashSet<>();
    /**
     * {@link OutputStream} for each file type
     */
    private OutputStream out = null;
    /**
     * Type of File enum
     */
    private FilterType fileType;

    /**
     * @param key Keyword for which singleton is created and {@link OutputStream}  is created
     * @throws IOException Throws exception if cannot initialize {@link OutputStream}
     */
    private WriteFilter(FilterType key) throws IOException {
        String outputFile = programSettingFolder + key.name() + ".lists";
        out = Files.newOutputStream(Paths.get(outputFile), CREATE);
        this.fileType = key;
    }

    /**
     * @param key Creates a Singleton for each {@link #fileType} and saves all instances to a map {@link #instanceMap}
     * @return A single instance for each {@link FilterType} is returned.
     * @throws IOException Throws exception if cannot initialize {@link OutputStream}
     */
    static WriteFilter getInstance(FilterType key) throws IOException {
        synchronized (WriteFilter.class) {
            if (!instanceMap.containsKey(key)) {
                instanceMap.put(key, new WriteFilter(key));
            }
        }
        return instanceMap.get(key);
    }

    /**
     * @param joiner Output row received from {@link WriteFilter} after matching with Keyword from {@link WriteFilter}
     * @throws IOException Throws exception if cannot initialize {@link OutputStream}
     */
    void write(String joiner) throws IOException {
        synchronized (instanceMap.get(fileType)) {
            existingList.add(joiner);
        }
    }

    /**
     * Closes {@link OutputStream} of this instance.
     */
    void close() {
        try {
            for (String line : existingList) {
                out.write((line + System.lineSeparator()).getBytes());
            }
            out.close();
            out = null;
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }
}