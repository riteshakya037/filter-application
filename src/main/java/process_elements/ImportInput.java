package process_elements;

import constant_elements.AppProperties;
import ui_elements.App;
import utility_elements.PropertyUtilities;
import utility_elements.RunOption;

import javax.swing.*;
import java.io.File;
import java.util.Collections;

/**
 * Provides all needed information and execution for Input Import
 * @author Ritesh Shakya
 */
public class ImportInput implements RunOption {

    /**
     * Adds Files to Upload List
     *
     * @param data Files array from the selected {@link JFileChooser#getSelectedFiles()}
     */
    public void execute(File[] data) {
        Collections.addAll(App.filesUploaded, data);
    }

    /**
     * @return Location of Input Folder used previously
     */
    public String getLocation() {
        return PropertyUtilities.getProperty(AppProperties.INPUT_LOCATION);
    }

    /**
     * @param currentDirectory Location of current processing
     */
    public void setLocation(String currentDirectory) {
        PropertyUtilities.saveProperty(AppProperties.INPUT_LOCATION, currentDirectory);
    }

}

