package utility_elements;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;

/**
 * Extension of {@link HashMap} that enables item listener.
 * @author Ritesh Shakya
 */
public class ListenerMap<K, V> extends HashMap<K, V> {

    /**
     * Property name of Listener
     */
    private static final String PROP_PUT = "put";
    /**
     * Manages a list of listeners and dispatches
     * {@link PropertyChangeEvent}s to them.
     */
    private PropertyChangeSupport propertySupport;

    public ListenerMap() {
        super();
        propertySupport = new PropertyChangeSupport(this);
    }

    /**
     * Adds Listener to {@link HashMap#put(Object, Object)}
     *
     * @param k Key Object
     * @param v Value Object
     * @return Implementation of {@link HashMap#put(Object, Object)} with action Listener.
     */
    @Override
    public V put(K k, V v) {
        V old = super.put(k, v);
        propertySupport.firePropertyChange(PROP_PUT, old, v);
        return old;
    }

    /**
     * Listener which triggers on adding element.
     *
     * @param listener Change Listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }

    /**
     * Listener which triggers on removing element.
     *
     * @param listener Change Listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
}
