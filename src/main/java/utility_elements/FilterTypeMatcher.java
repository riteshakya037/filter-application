package utility_elements;

import constant_elements.FilterType;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Matches given string to {@link FilterType} and returns the {@link FilterType}.
 * @author Ritesh Shakya
 */
public class FilterTypeMatcher {
    private final Pattern pattern;
    private final FilterType type;

    /**
     * @param type {@link FilterType} to check
     */
    public FilterTypeMatcher(FilterType type) {
        pattern = type.getPattern();
        this.type = type;
    }

    /**
     * Returns the {@link FilterType} that the string matches with.
     *
     * @param s String to check
     * @return {@link FilterType} that the string matches with.
     */
    public FilterType match(String s) {
        Matcher m = pattern.matcher(s);
        return m.matches() ? type : null;
    }

}