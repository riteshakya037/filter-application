package utility_elements;

import constant_elements.AppProperties;

import java.io.*;
import java.util.Properties;

import static utility_elements.MavenProperties.getProjectProperties;

/**
 * Sets and gets various program properties.
 * @author Ritesh Shakya
 */
public class PropertyUtilities {
    /**
     * Location of config file.
     */
    private static final String configFile = System.getProperty("user.home") + File.separator + getProjectProperties("name") + File.separator + "config.properties";
    /**
     * Folder location of Program Data.
     */
    private static final String programSettingFolder = System.getProperty("user.home") + File.separator + getProjectProperties("name");

    /**
     * @param properties Enum of certain Program Configurations
     * @param value      value to save to property defined by {@code properties}
     */
    public static void saveProperty(AppProperties properties, String value) {
        Properties prop = new Properties();
        File folder = new File(programSettingFolder);
        if (!folder.exists()) {
            folder.mkdir();
        }

        OutputStream output = null;
        try {
            try (FileInputStream in = new FileInputStream(configFile)) {
                prop.load(in);
            } catch (FileNotFoundException ignored) {
            }
            output = new FileOutputStream(configFile);
            prop.setProperty(properties.getValue(), value);
            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * @param properties Enum of certain Program Configurations
     * @return String value of {@link AppProperties} as stored in {#link configFile}
     * @see AppProperties
     */
    public static String getProperty(AppProperties properties) {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(configFile);
            prop.load(input);
            return prop.getProperty(properties.getValue(), "");
        } catch (IOException ignored) {
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }
}
