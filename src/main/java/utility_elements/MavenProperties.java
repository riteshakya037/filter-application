package utility_elements;

import java.io.IOException;
import java.util.Properties;

/**
 * Class that gets specific properties from pom.xml
 * @author Ritesh Shakya
 */
public class MavenProperties {
    /**
     * Gets property from pom.xml
     *
     * @param property Project property name. e.g., version,name
     * @return value associated with the key defined by {@code property}
     */
    public static String getProjectProperties(String property) {
        final Properties properties = new Properties();
        try {
            properties.load(MavenProperties.class.getResourceAsStream("/project.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(property);
    }
}
