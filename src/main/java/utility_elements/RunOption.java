package utility_elements;

import javax.swing.*;
import java.io.File;

/**
 * Interface to implement variable options on output of FileChooser
 * @author Ritesh Shakya
 */
public interface RunOption {

    /**
     * Executes the respective Files
     *
     * @param data Files array from the selected {@link JFileChooser#getSelectedFiles()}
     */
    void execute(File[] data);

    /**
     * Gets location of previous processing from Property File
     *
     * @return Location of Folder used previously
     */
    String getLocation();

    /**
     * Stores location of current processing in Property File
     *
     * @param currentDirectory Location of current processing
     */
    void setLocation(String currentDirectory);
}