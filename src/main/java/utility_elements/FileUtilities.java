package utility_elements;

import constant_elements.AppProperties;
import constant_elements.Constants;
import constant_elements.FilterType;
import ui_elements.App;
import ui_elements.IndividualFilter;

import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Stream;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Contains a collection of method which deals with files
 *
 * @author Ritesh Shakya
 */
public class FileUtilities {

    /**
     * Default location to store program settings
     */
    private static final String programSettingFile = PropertyUtilities.getProperty(AppProperties.FILTER_FOLDER) + File.separator + "filter_file.lists";
    private static final String filterFolder = PropertyUtilities.getProperty(AppProperties.FILTER_FOLDER) + File.separator + "Filter Files";
    private static final String programSettingFolder = PropertyUtilities.getProperty(AppProperties.FILTER_FOLDER) + File.separator;
    /**
     * Separator used in {@link #programSettingFile} to separate columns
     */
    private static String hashSeparator = "::";

    /**
     * Reads list of available Filter Files.
     *
     * @return List of Filter Files stored in Program.
     */
    public static ListenerMap<String, Boolean> readFilterFiles() {
        ListenerMap<String, Boolean> filterList = new ListenerMap<>();
        try (Stream<String> lines = Files.lines(Paths.get(programSettingFile), StandardCharsets.UTF_8)) {
            for (String line : (Iterable<String>) lines::iterator) {
                String[] splitLine = line.split(hashSeparator);
                filterList.put(splitLine[0], Boolean.valueOf(splitLine[1]));
            }
        } catch (IOException ignored) {
        }
        return filterList;
    }

    /**
     * Reads all the filters for specific {@link FilterType}
     *
     * @param filterType FilterType for which filters are to be read.
     * @return List of all the filters for specific file type.
     */
    public static Stream<String> readFilters(FilterType filterType) {
        try  {
            return Files.lines(Paths.get(programSettingFolder + filterType.name() + ".lists"), StandardCharsets.UTF_8);
        } catch (IOException ignored) {
        }
        return Stream.empty();
    }

    /**
     * Writes out list of available Filter Files.
     *
     * @param components List of Filter Files in UI form.
     */
    public static void writeFilterFiles(Component[] components) {
        try (PrintWriter out = new PrintWriter(new FileWriter(programSettingFile))) {
            for (Component component : components) {
                IndividualFilter individualFilter = (IndividualFilter) component;
                out.write(individualFilter.getKeyword() + hashSeparator + individualFilter.getUseFor() + System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }

    /**
     * Copy Filter Files to Filter Folder.
     *
     * @param file File to copy.
     */
    public static void copyToFilter(File file) {
        Path source = Paths.get(file.getAbsolutePath());
        Path newDir = Paths.get(filterFolder);
        try {
            if (!Files.exists(newDir))
                Files.createDirectories(newDir);
            Files.copy(source, newDir.resolve(source.getFileName()), REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete all Filter Lists.
     */
    public static void removeFilterList() {
        for (FilterType filterType : Arrays.asList(FilterType.values())) {
            delete(programSettingFolder + filterType.name() + ".lists");
        }
    }

    /**
     * Delete a file
     *
     * @param filePath File to delete.
     */
    private static void delete(String filePath) {
        try {
            Files.deleteIfExists(Paths.get(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes a specific Filter File stored in Filter Folder.
     *
     * @param fileName Delete Filter File.
     */
    public static void deleteFilter(String fileName) {
        delete(filterFolder + File.separator + fileName);
    }

    /**
     * Opens {@link OutputStream} for Logging files and sets it as default System.out.
     */
    public static void setLog() {
        String logOutput = App.getOutputDirectory() + Constants.LOG_DIRECTORY;
        if (!(new File(logOutput)).exists()) {
            (new File(logOutput)).mkdir();
        }
        try {
            System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream(logOutput + new SimpleDateFormat("'log-'yyyy-MM-dd hh-mm-ss'.tsv'").format(new Date()))), true));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Moved failed files to Failed Folder.
     *
     * @param absolutePath Path to file to move.
     */
    public static void moveFailed(String absolutePath) {
        String failedFolder = App.getOutputDirectory() + Constants.FAILED_DIRECTORY;
        if (!(new File(failedFolder)).exists()) {
            (new File(failedFolder)).mkdir();
        }
        try {
            Files.copy(
                    Paths.get(absolutePath),
                    Paths.get(failedFolder).resolve(Paths.get(absolutePath).getFileName()), REPLACE_EXISTING
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
